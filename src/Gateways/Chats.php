<?php

namespace Titanium\Gateways;

use Titanium\Titanium;

class Chats
{
    private $master = null;

    public function __construct(Titanium $master)
    {
        $this->master = $master;
    }

    /**
     * Total number of Chat objects. Returned in the meta header.
     */
    public function count()
    {
        $response = $this->master->call('chats/count', [], 'GET');

        if (! array_key_exists('meta', $response)) {
            throw new \RuntimeException(
                sprintf(
                    "Couldn't receive chats count result: %s",
                    print_r($response, true)
                )
            );
        }

        return array_key_exists('count', $response['meta'])
            ? $response['meta']['count']
            : 0;
    }

    /**
     * Sends a chat message to another user or a group of users.Sending a
     * message creates a new chat group if there is no existing chat group
     * containing the current user and the designated recipients.To generate
     * a push notification, include the channel and payload parameters in the
     * array.
     */

    public function create(array $params)
    {
        return $this->master->call('chats/create', $params);
    }

    /**
     * Deletes a chat message.
     */

    public function delete($id, $su_id = null)
    {
        $params['chat_id'] = $id;

        if (! empty($su_id)) {
            $params['su_id'] = $su_id;
        }

        return $this->master->call('chats/delete', $params, 'DELETE');
    }

    /**
     * Lists chat groups.If user A sends chat message to user B and C, users A,
     * B and C automatically form a chat group. Use this API to get a list of
     * chat groups the current user belongs to.
     */

    public function get_chat_groups(array $params = array())
    {
        return $this->master->call('chats/get_chat_groups',$params, 'GET');
    }

    /**
     * Performs a custom query of chat messages with sorting and pagination.
     * Currently you can not query or sort data stored inside array or hash in
     * custom fields.
     */

    public function query(array $params = array())
    {
        return $this->master->call('chats/query', $params, 'GET');
    }
}

<?php

namespace Titanium;

define('VERSION', '1.0');
define('USERAGENT', 'Titanium-PHP/' . VERSION);
define('CONTENTTYPE','Content-Type: application/json');

use Titanium\Gateways\Users;
use Titanium\Gateways\Chats;
use Titanium\Gateways\Posts;

class Titanium
{
    public $apikey;
    public $ch;
    public $root = 'https://api.cloud.appcelerator.com/v1';
    public $debug = false; //ainda nao foi muito util ligado
    public $meta = null;
    public $response = null;

    public static $error_map = [
        "fail"               => Titanium_Fail::class,
        "HttpError"          => Titanium_Error::class,
        "ServiceUnavailable" => Titanium_ServiceUnavailable::class,
        "Invalid_Key"        => Titanium_Invalid_Key::class,
        "Unknown_Message"    => Titanium_Unknown_Message::class,
    ];

    public function __construct($apikey = null)
    {
        if(!$apikey) $apikey = getenv('TITANIUM_APIKEY');
        if(!$apikey) $apikey = $this->readConfigs();
        if(!$apikey) throw new Titanium_Error('You must provide an Appcelerator Titanium API key');
        $this->apikey = $apikey;

        $this->ch = curl_init();

        $this->setCurlOptions();

        $this->root = rtrim($this->root, '/') . '/';

        $this->users = new Users($this);
        $this->chats = new Chats($this);
        $this->posts = new Posts($this);
    }

    public function setCurlOptions()
    {
        curl_setopt($this->ch, CURLOPT_USERAGENT, USERAGENT);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0); //@TODO eu sei que nao deveria fazer isso, culpa do appcelerator
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    public function call($url, $params, $requestMethod = 'POST')
    {
        if (isset($_SESSION['curl_cookie'])) {
            $cookie = $_SESSION['curl_cookie'];
        } else {
            $cookie = '';
        }
        print $cookie . "<br>";

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(CONTENTTYPE, "Cookie: " . $cookie));

        if ((count($params) != 0) && $requestMethod == 'POST') {                  //if post
            $params = json_encode($params);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($this->ch, CURLOPT_POST, true);
            $params = '';
        } elseif ($requestMethod == "DELETE") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $params = $this->buildQueryString($params);
            // $params = json_encode($params);
            // curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
        } else { // else get
            $params = $this->buildQueryString($params);
            print "Params: $params<br>";
            curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        }

        curl_setopt($this->ch, CURLOPT_URL, $this->root . $url . '.json' . '?key=' . $this->apikey . $params); //preparing the URL

        //log block start
        $start = microtime(true);
        $this->log('Call to ' . $this->root . $url . '.json: ' . $params);
        if($this->debug) {
            $curl_buffer = fopen('php://memory', 'w+');
            curl_setopt($this->ch, CURLOPT_STDERR, $curl_buffer);
        }

        $response_body = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        $time = microtime(true) - $start;
        if($this->debug) {
            rewind($curl_buffer);
            $this->log(stream_get_contents($curl_buffer));
            fclose($curl_buffer);
        }
        $this->log('Completed in ' . number_format($time * 1000, 2) . 'ms');
        $this->log('Got response: ' . $response_body);
        //log block end


        if(curl_error($this->ch)) {
            throw new Titanium_HttpError("API call to $url failed: " . curl_error($this->ch));
        }

        $result = json_decode($response_body, true);
        if($result === null) throw new Titanium_Error('We were unable to decode the JSON response from the Titanium API: ' . $response_body);

        $this->meta = $result['meta'];
        if(floor($info['http_code'] / 100) >= 4) {
            throw $this->castError($result);
        }

        $this->response = array_key_exists('response', $result)
            ? $result['response']
            : $result;

        $this->prepareCookie($result);

        return $this->response;
    }

    public function readConfigs()
    {
        $paths = array('~/.titanium.key', '/etc/titanium.key');
        foreach($paths as $path) {
            if(file_exists($path)) {
                $apikey = trim(file_get_contents($path));
                if($apikey) return $apikey;
            }
        }
        return false;
    }

    public function castError($result)
    {
        $class = (isset(self::$error_map[$result['meta']['status']])) ? self::$error_map[$result['meta']['status']] : 'Titanium_Unknown_Message';
        return new $class($result['meta']['message'], $result['meta']['code']);
    }

    public function log($msg)
    {
        if($this->debug) error_log($msg);
    }

    /**
     * A private function to save the id_session and others informations from the curl's result for the next call
     * @param array $result
     */
    private function prepareCookie($result)
    {
        $curl_cookie = "";
        if((!isset($_SESSION['curl_cookie'])) && (isset($result['meta']['session_id']))) {
            $curl_cookie = '_session_id=' . $result['meta']['session_id'];
            $_SESSION['curl_cookie'] = $curl_cookie;
        }
    }

    public function buildQueryString(array $params)
    {
        return is_array($params) && count($params) > 0
            ? '&' . http_build_query($params)
            : '';
    }
}

